Sự trưởng thành nào cũng có khởi nguồn và người đặt nền móng. Năm 2013, sau thời gian dài gắn bó cùng Vinhomes, nhận thấy những giá trị vượt bậc mà các dự án của Vinhomes có thể đem lại cho cuộc sống người Việt, ông [Hoàng Đình Khiêm](https://www.behance.net/gallery/87122269/Hoang-Dinh-Khiem) đã tràn đầy khát vọng được trở thành cầu nối để đông đảo khách hàng có cơ hội trải nghiệm phong cách sống mới hiện đại, sang trọng. Từ đây, Vietstarland đã được ra đời và trở thành đơn vị đầu tiên hợp tác cùng Vinhomes, phân phối các sản phẩm bất động sản cao cấp ra thị trường.

**[Ông Hoàng Đình Khiêm](https://hoangdinhkhiem.livejournal.com/369.html)** được nhiều người biết đến là doanh nhân trẻ có cơ duyên đặc biệt với khu đô thị Vinhomes Riverside và là người đồng hành thổi hồn vào dự án Vinhomes Riverside từ ngày đào móng. Khởi nghiệp từ Vinhomes Riverside, cái tên “Khiêm Vinhomes” đã trở nên thân thuộc với đông đảo khách hàng, đối tác và cùng gắn liền với sự phát triển vững mạnh của Vietstarland – niềm tự hào, tâm huyết và khát vọng tiên phong của ông.

Từ 5 thành viên ở thời điểm thành lập, đến nay Vietstarland đã phát triển lên tới 500 thành viên với 6 văn phòng đại diện tại các dự án. Bên cạnh đó là kết quả kinh doanh đáng kinh ngạc, các con số căn hộ, biệt thự được phân phối bởi Vietstarland tăng dần theo từng tháng.

Năm 2015, Vietstarland là đại lý đem về doanh thu lớn cho Vinhomes và vinh dự nhận được giải thưởng đại lý bứt phá từ chủ đầu tư Vingroup. Nửa năm đầu 2016, Vietstarland tiếp tục giữ vững vị trí là đơn vị phân phối hàng đầu các sản phẩm BĐS Vinhomes tại các dự án: Vinhomes Riverside, Vinhomes Thăng Long, Vinhomes Metropolis, Vinpearl Resort & Villas. Và hứa hẹn sẽ nối dài chuỗi thành tích tại các dự án tới đây như: Vinhomes Riverside giai đoạn 2, Vinhomes Trần Duy Hưng, Vinhomes Mễ Trì…

Địa chỉ: TTGD BĐS Vinhomes, số 10 đường Tương Lai, hầm B1, Times City, Phố Minh Khai, Vĩnh Tuy, Hai Bà Trưng, Hà Nội

Email: vsl.hoangdinhkhiem@gmail.com

Điện thoại: 096 466 88 88
>>> Xem thêm về CEO Vietstarland Hoàng Đình Khiêm: [https://about.me/hoangdinhkhiem](https://about.me/hoangdinhkhiem)